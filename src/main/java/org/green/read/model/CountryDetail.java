package org.green.read.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class CountryDetail implements Serializable {
    private String name;
    List<String> topLevelDomain = new ArrayList<>();
    private String alpha2Code;
    private String alpha3Code;
    List<String> callingCodes = new ArrayList<>();
    private String capital;
    List<String> altSpellings = new ArrayList<>();
    private String region;
    private String subregion;
    private float population;
    List<String> latlng = new ArrayList<>();
    private String demonym;
    private float area;
    private float gini;
    List<String> timezones = new ArrayList<>();
    List<String> borders = new ArrayList<>();
    private String nativeName;
    private String numericCode;
    List<Map<String, String>> currencies = new ArrayList<>();
    List<Map<String, String>> languages = new ArrayList<>();
    Translations TranslationsObject;
    private String flag;
    List<Map<String, Object>> regionalBlocs = new ArrayList<>();
    private String cioc;
}


