package org.green.read.model;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class Translations{
    private String de;
    private String es;
    private String fr;
    private String ja;
    private String it;
    private String br;
    private String pt;
    private String nl;
    private String hr;
    private String fa;
}
