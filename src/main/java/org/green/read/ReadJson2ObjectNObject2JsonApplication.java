package org.green.read;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.green.read.model.CountryDetail;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@SpringBootApplication
public class ReadJson2ObjectNObject2JsonApplication implements CommandLineRunner {

	public void getAllCountryCodes() throws Exception {
        // Uncomment the below if running directly from intellij
//		String fileLocation = "D:\\Users\\sm13\\Documents\\development\\read-json-2-object-n-object-2-json\\";

		// Uncommnent the below if running from command line like "mvn spring-boot:run"
		String fileLocation = "";
		ObjectMapper mapper = new ObjectMapper();
		File jsonFile = new File(fileLocation + "listCountries.json");
		log.info("file: {}", jsonFile.getAbsolutePath());
		CountryDetail[] countries = mapper.readValue(jsonFile, CountryDetail[].class);
		Map<String, CountryDetail> countryCodeRecords = new HashMap<>();

		for (CountryDetail detail : countries) {
			countryCodeRecords.put(detail.getAlpha2Code(), detail);
		}
		mapper.writerWithDefaultPrettyPrinter().writeValue(new FileOutputStream(fileLocation + "listCountries2.json"), countryCodeRecords);
	}

	@Override
	public void run(String... args) throws Exception {
		getAllCountryCodes();
	}

	public static void main(String[] args) {
		SpringApplication.run(ReadJson2ObjectNObject2JsonApplication.class, args);
	}

}
